import Vue from "vue";
import Router from "vue-router";
import Principal from "./components/Principal";
import Detalles from "./components/Detalles";
import Insertar from "./components/Insertar";
import Modificar from "./components/Modificar";

Vue.use(Router);

export default new Router({
	mode: "history",
	routes: [
		{
			path: "/",
			name: "Principal",
			component: Principal,
		},
		{
			path: "/detalles/:id",
			name: "Detalles",
			component: Detalles,
		},
		{
			path: "/modificar/:id",
			name: "Modificar",
			component: Modificar,
		},
		{
			path: "/insertar",
			name: "Insertar",
			component: Insertar,
		},
	],
});
